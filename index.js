const http = require('http')
const fs = require('fs')
const { v4: uuidv4 } = require('uuid')

const server = http.createServer(function (request, response) {
    const url = request.url.split('/')
    if (request.url === '/html') {
        fs.readFile('htmlProblem.html', 'utf-8', (err, data) => {
            if (err) {
                response.writeHead(500, { 'Content-Type': 'text/html' })
                response.end('<h1>Internal Server Error</h1>')
            } else {
                response.writeHead(200, { 'Content-Type': 'text/html' })
                response.end(data)
            }
        })
    } else if (request.url === '/json') {
        fs.readFile('jsonProblem.json', 'utf-8', (err, data) => {
            if (err) {
                response.writeHead(500, { 'Content-Type': 'text/html' })
                response.end('<h1>Internal Server Error</h1>')
            } else {
                response.writeHead(200, { 'Content-Type': 'application/json' })
                response.end(data)
            }
        })
    } else if (request.url === '/uuid') {
        response.writeHead(200, { 'Content-Type': 'application/json' })
        response.end(JSON.stringify({ 'uuid': uuidv4() }))

    } else if (url[1] === 'status' && isNaN(url[2]) === false && url.length === 3) {
        if (url[2] > 599 || url[2] < 100) {
            response.writeHead(500, { 'Content-Type': 'text/html' })
            response.end('<h1>Internal Server Error</h1>\n <h3>Status code range Error</h3>')
        } else {
            if (url[2] > 99 && url[2] < 200) {
                response.writeHead(200, { 'Content-Type': 'text/html' })
                response.end(`<h1>Status Code : ${url[2]}</h1>`);
            } else {
                response.writeHead(url[2], { 'Content-Type': 'text/html' })
                response.end(`<h1>Status Code : ${url[2]}</h1>`)
            }
        }
    } else if (url[1] === 'delay' && isNaN(url[2]) === false && url.length === 3) {
        setTimeout(() => {
            response.writeHead(200, { 'Content-Type': 'text/html' })
            response.end(`<h1> Successful response after ${url[2]} seconds</h1>`)
        }, Number(url[2]) * 1000)
    } else {
        response.writeHead(404, { 'Content-Type': 'text/html' })
        response.end('<h1>404 Page Not Found</h1>')
    }
})

server.listen(3000, () => {
    console.log('server is running on 3000 port')
})